/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hepta.setores.rest;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author mateus.marques
 */
@ApplicationPath("restapi")
public class RestServices  extends Application{
    
    private final Set<Object> singletons = new HashSet<>();

    public RestServices() {
        this.singletons.add(new FuncionarioService());
    }
    
    @Override
    public Set<Object> getSingletons(){
        return singletons;
    }
    
    
    
    
}
