package com.hepta.funcionarios.persistence;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {
	private static final String PERSISTENCE_UNIT_NAME = "heptabdPU";
	
	private static EntityManagerFactory factory;

	public static EntityManagerFactory getEntityManagerFactory() {
		if (factory == null) {
			createEntityManagerFactory();
		}
		return factory;
	}

	public static void shutdown() {
		if (factory != null) {
			factory.close();
		}
	}
	
	public static void createEntityManagerFactory(){
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}
	
	public static EntityManager getEntityManager(){
		return HibernateUtil.getEntityManagerFactory().createEntityManager();
	}
	
	public static void main(String[] args) {
                
                Funcionario funcionario = new Funcionario();
                Setor setor = new Setor();
                
                setor.setId(null);
                setor.setNome("qualquer");
                funcionario.setId(null);
                funcionario.setEmail("blabla");
                
                
                funcionario.setSetor(setor);
                funcionario.setIdade(25);
                funcionario.setNome("Mateus");
                funcionario.setSalario(13.3);
                
		EntityManager em =  HibernateUtil.getEntityManagerFactory().createEntityManager();
                
                em.getTransaction().begin();
                em.persist(funcionario);
                em.persist(setor);
                em.getTransaction().commit();

                
                HibernateUtil.shutdown();
                
	}
}
